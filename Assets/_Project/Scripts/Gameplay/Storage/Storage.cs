using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Gameplay.ProductLogic;
using _Project.Scripts.Global;
using UnityEngine;

namespace _Project.Scripts.Gameplay.Storage
{
    public class Storage : BaseStorage
    {
        [field: SerializeField] public Constants.StorageType StorageType { get; private set; }
        [field: SerializeField] public Constants.ResourceType ResourceType { get; private set; }
        [SerializeField] private int _capacity;
        [SerializeField] private List<Product> _products = new List<Product>();
        [SerializeField] private Transform _parent;
        private float _productHeight = 0.11f;
        private float _currentHeight = 0.5f;


        public override void AddResource(Product product)
        {
            if (_products.Count + 1 < _capacity)
            {
                StackProducts(product);
                _products.Add(product);
            }
        }

        public override void RemoveResource(Product product, bool needToDestroyProduct)
        {
            if (_products.Count == 0)
            {
                return;
            }
            
            if (needToDestroyProduct && product)
            {
                Destroy(product.ProductGameObject);
            }

            _products.Remove(product);

            IncreaseCurrentHeight();
        }

        public override Product GetProduct()
        {
            if (_products.Count > 0)
            {
                return _products[_products.Count - 1];
            }

            return null;
        }

        public int GetResourceAmount(Constants.ResourceType type)
        {
            return _products.Count(x => (x.ResourceType & type) != 0);
        }

        public int GetResourceAmount()
        {
            return _products.Count;
        }

        public bool IsEnoughSpaceInStorage()
        {
            return _products.Count < _capacity - 1;
        }

        private void StackProducts(Product product)
        {
            product.transform.position = new Vector3(_parent.position.x, _currentHeight + _productHeight, _parent.position.z);
            product.transform.rotation = _parent.parent.transform.rotation;
            _currentHeight += _productHeight;
            product.transform.parent = _parent;
        }

        private void IncreaseCurrentHeight()
        {
            if (_currentHeight > 0)
                _currentHeight -= _productHeight;

            if (_currentHeight < 0)
                _currentHeight = 0;
        }
    }
}