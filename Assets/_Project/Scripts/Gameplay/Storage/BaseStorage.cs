using _Project.Scripts.Gameplay.ProductLogic;
using UnityEngine;

namespace _Project.Scripts.Gameplay.Storage
{
    public abstract class BaseStorage : MonoBehaviour
    {
        public abstract void AddResource(Product product);
        public abstract void RemoveResource(Product product, bool needToDestroyProduct);
        public abstract Product GetProduct();
    }
}
