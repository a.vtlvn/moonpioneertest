using UnityEngine;

namespace _Project.Scripts.Gameplay.Factory
{
    public abstract class BaseFactory : MonoBehaviour
    {
        public abstract void Produce();
    }
}
