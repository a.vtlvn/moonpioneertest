using UnityEngine;

namespace _Project.Scripts.Gameplay.Factory
{
    public class FactoryManager : MonoBehaviour
    {
        [SerializeField] private BaseFactory[] _factories;

        private void Start()
        {
            foreach (var factory in _factories)
            {
                factory.Produce();
            }
        }
    }
}
