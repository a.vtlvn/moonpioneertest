using System.Collections;
using _Project.Scripts.Gameplay.ProductLogic;
using _Project.Scripts.Global;
using _Project.Scripts.UI;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.Gameplay.Factory
{
    public class Factory2 :BaseFactory
    {
        [SerializeField] private int _produceTime;
        [SerializeField] private Storage.Storage _inputStorage;
        [SerializeField] private Storage.Storage _outputStorage;
        [SerializeField] private Product _producedProductPrefab;
        [Inject] private InfoText _infoText;
        private bool _messageIsShown;

        
        public override void Produce()
        {
            StartCoroutine(ProduceRoutine());
        }

        private IEnumerator ProduceRoutine()
        {
            while (true)
            {
                yield return null;
                
                if (_outputStorage.IsEnoughSpaceInStorage() && _inputStorage.GetResourceAmount(Constants.ResourceType.Resource1) > 1)
                {
                    yield return new WaitForSeconds(_produceTime);
                    MoveProductToFactory();
                    CreateProduct();
                    _messageIsShown = false;
                }
                else
                {
                    ShowInfoMessage();
                }
            }
        }

        private void ShowInfoMessage()
        {
            if (_messageIsShown)
                return;

            _messageIsShown = true;
            
            if (!_outputStorage.IsEnoughSpaceInStorage())
            {
                _infoText.ShowInfoText(GetType().Name + " " + Constants.NOT_ENOUGH_SPACE_IN_THE_OUTPUT_STORAGE);
            }
            else if (_inputStorage.GetResourceAmount(Constants.ResourceType.Resource1) < 1)
            {
                _infoText.ShowInfoText(GetType().Name + " " + Constants.NOT_ENOUGH_RESOURCES + " " + Constants.ResourceType.Resource1);
            }
        }

        private void MoveProductToFactory()
        {
            var product = _inputStorage.GetProduct();
            product.ProductMove.Move(_inputStorage.transform.position, transform.position, Quaternion.identity, Quaternion.identity, () => RemoveProductFromStorage(product));
        }

        private void CreateProduct()
        {
            Product product = Instantiate(_producedProductPrefab, transform.position, Quaternion.identity);
            product.ProductMove.Move(transform.position, _outputStorage.transform.position, Quaternion.identity, Quaternion.identity, () => AddProductToStorage(product));
        }
        
        private void AddProductToStorage(Product product)
        {
            _outputStorage.AddResource(product);
        }

        private void RemoveProductFromStorage(Product product)
        {
            _inputStorage.RemoveResource(product, true);
        }
    }
}
