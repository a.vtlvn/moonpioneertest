using System.Collections;
using _Project.Scripts.Gameplay.ProductLogic;
using _Project.Scripts.Global;
using _Project.Scripts.UI;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.Gameplay.Factory
{
    public class Factory1 : BaseFactory
    {
        [SerializeField] private int _produceTime;
        [SerializeField] private Storage.Storage _outputStorage;
        [SerializeField] private Product _producedProductPrefab;
        [Inject] private InfoText _infoText;
        private int _createdProductCount;
        private bool _messageIsShown;

        public override void Produce()
        {
            StartCoroutine(ProduceRoutine());
        }

        private IEnumerator ProduceRoutine()
        {
            while (true)
            {
                yield return null;
                
                if (_outputStorage.IsEnoughSpaceInStorage())
                {
                    yield return new WaitForSeconds(_produceTime);
                    CreateProduct();
                    _messageIsShown = false;
                }
                else
                {
                    if (!_messageIsShown)
                    {
                        _messageIsShown = true;
                        _infoText.ShowInfoText(GetType().Name + " " + Constants.NOT_ENOUGH_SPACE_IN_THE_OUTPUT_STORAGE);
                    }
                }
            }
        }

        private void CreateProduct()
        {
            Product product = Instantiate(_producedProductPrefab, transform.position, Quaternion.identity);
            product.ProductMove.Move(transform.position, _outputStorage.transform.position, Quaternion.identity, Quaternion.identity, () => AddProductToStorage(product));
            _createdProductCount++;
            product.transform.name = "Product " + GetType().Name + "_" + _createdProductCount;
        }

        private void AddProductToStorage(Product product)
        {
            _outputStorage.AddResource(product);
        }
    }
}
