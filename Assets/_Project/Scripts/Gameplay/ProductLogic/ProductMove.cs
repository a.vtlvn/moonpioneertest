using System;
using _Project.Scripts.Extension;
using UnityEngine;

namespace _Project.Scripts.Gameplay.ProductLogic
{
    public class ProductMove : MonoBehaviour
    {
        [SerializeField] private float _movementDuration;

        public void Move(Vector3 startPoint, Vector3 endPoint, Quaternion playerRotStart, Quaternion playerRotEnd, Action onMovementFinished)
        {
            StartCoroutine(
                LerpEnumerator.OnUpdate(
                    _movementDuration,
                    (t) =>
                    {
                        transform.position = Vector3.Lerp(startPoint, endPoint, t);
                        transform.rotation = Quaternion.Lerp(playerRotStart, playerRotEnd, t);
                    },
                    onMovementFinished
                ));
        }
    }
}