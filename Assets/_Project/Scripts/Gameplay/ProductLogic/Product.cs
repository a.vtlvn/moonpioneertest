using _Project.Scripts.Global;
using UnityEngine;

namespace _Project.Scripts.Gameplay.ProductLogic
{
    public class Product : MonoBehaviour
    {
        public ProductMove ProductMove;
        public GameObject ProductGameObject;
        public Constants.ResourceType ResourceType;
    }
}