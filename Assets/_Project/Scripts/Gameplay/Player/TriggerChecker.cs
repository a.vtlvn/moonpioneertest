using System;
using System.Collections;
using _Project.Scripts.Gameplay.ProductLogic;
using _Project.Scripts.Global;
using UnityEngine;

namespace _Project.Scripts.Gameplay.Player
{
    public class TriggerChecker : MonoBehaviour
    {
        [SerializeField] private Transform _stackPoint;
        [SerializeField] private Storage.Storage _playerStorage;
        private IEnumerator _coroutine;
        private float _productCheckTime = 0.75f;


        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(Constants.STORAGE_TAG))
            {
                Storage.Storage storage = other.GetComponentInChildren<Storage.Storage>();
                CheckStorageType(storage);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag(Constants.STORAGE_TAG))
            {
                if (_coroutine != null)
                    StopCoroutine(_coroutine);
            }
        }

        private void CheckStorageType(Storage.Storage storage)
        {
            switch (storage.StorageType)
            {
                case Constants.StorageType.Input:
                    InputProducts(storage, _playerStorage);
                    break;
                case Constants.StorageType.Output:
                    OutputProducts(_playerStorage, storage);
                    break;
            }
        }

        private void OutputProducts(Storage.Storage storageToAdd, Storage.Storage storageToRemove)
        {
            _coroutine = OutputProductsRoutine(storageToAdd, storageToRemove);
            StartCoroutine(_coroutine);
        }
        
        private void InputProducts(Storage.Storage storageToAdd, Storage.Storage storageToRemove)
        {
            _coroutine = InputProductsRoutine(storageToAdd, storageToRemove);
            StartCoroutine(_coroutine);
        }

        private IEnumerator OutputProductsRoutine(Storage.Storage storageToAdd, Storage.Storage storageToRemove)
        {
            while (storageToRemove.GetResourceAmount() > 0)
            {
                yield return new WaitForSeconds(_productCheckTime);
                
                Product product = storageToRemove.GetProduct();
            
                if (product != null)
                {
                    product.ProductMove.Move(product.transform.position, transform.position, product.transform.rotation, _stackPoint.rotation,  
                        () => UpdateProducts(storageToAdd, storageToRemove, product));       
                }
            }
        }
        
        private IEnumerator InputProductsRoutine(Storage.Storage storageToAdd, Storage.Storage storageToRemove)
        {
            while (storageToRemove.GetResourceAmount() > 0 && storageToRemove.GetResourceAmount(storageToAdd.ResourceType) > 0)
            {
                yield return new WaitForSeconds(_productCheckTime);
                Product product = storageToRemove.GetProduct();
            
                if (product != null)
                {
                    product.ProductMove.Move(transform.position, storageToAdd.transform.position, product.transform.rotation, _stackPoint.rotation,  
                        () => UpdateProducts(storageToAdd, storageToRemove, product));       
                }
            }
        }
        
        private void UpdateProducts(Storage.Storage storageToAdd, Storage.Storage storageToRemove, Product product)
        {
            storageToRemove.RemoveResource(product, false);
            storageToAdd.AddResource(product);
        }
    }
}
