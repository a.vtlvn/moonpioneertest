using TMPro;
using UnityEngine;

namespace _Project.Scripts.UI
{
    public class InfoText : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _infoText;

        public void ShowInfoText(string text)
        {
            _infoText.text = text;
        }
    }
}
