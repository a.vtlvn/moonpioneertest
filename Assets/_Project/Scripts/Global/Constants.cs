using System;

namespace _Project.Scripts.Global
{
    public class Constants 
    {
        public const string GAME_SCENE_NAME = "Game";
        public const float EPSILON = 0.001f;
        
        //tags
        public const string STORAGE_TAG = "Storage";
        
        //info texts
        public const string NOT_ENOUGH_SPACE_IN_THE_OUTPUT_STORAGE = "Not enough space in the output storage";
        public const string NOT_ENOUGH_RESOURCES = "Not enough resources";
        
        [Flags]
        public enum ResourceType
        {
            None = 0,
            Resource1 = 1 << 0,
            Resource2 = 1 << 1,
            Resource3 = 1 << 2
        }
        
        public enum StorageType
        {
            None,
            Input,
            Output
        }
    }
}
