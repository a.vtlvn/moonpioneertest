using _Project.Scripts.Global;
using Zenject;

namespace _Project.Scripts.StateMachine
{
    public class BootstrapState : IState
    {
        private readonly IGameStateMachine _gameStateMachine;

        
        [Inject]
        public BootstrapState(IGameStateMachine gameStateMachine)
        {
            _gameStateMachine = gameStateMachine;
        }
        
        public void Enter()
        {
            _gameStateMachine.Enter<LoadSceneState, string>(Constants.GAME_SCENE_NAME);
        }

        public void Exit()
        {
            
        }
    }
}
