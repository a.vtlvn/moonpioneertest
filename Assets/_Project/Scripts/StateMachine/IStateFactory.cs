namespace _Project.Scripts.StateMachine
{
    public interface IStateFactory
    {
        T Create<T>() where T : IExitableState;
    }
}