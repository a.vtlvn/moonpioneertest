using UnityEngine.SceneManagement;
using Zenject;

namespace _Project.Scripts.StateMachine
{
    public class LoadSceneState : IPayLoadedState<string>
    {
        private readonly IGameStateMachine _gameStateMachine;
        
        [Inject]
        public LoadSceneState(IGameStateMachine gameStateMachine)
        {
            _gameStateMachine = gameStateMachine;
        }
        public void Enter(string payload)
        {
            SceneManager.LoadScene(payload);
            _gameStateMachine.Enter<GameState>();
        }

        public void Exit()
        {
        
        }
    }
}