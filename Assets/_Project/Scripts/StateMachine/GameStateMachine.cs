using System;
using System.Collections.Generic;
using Zenject;

namespace _Project.Scripts.StateMachine
{
    public class GameStateMachine : IGameStateMachine
    {
        private readonly IStateFactory _statesFactory;
        private Dictionary<Type, IExitableState> _states;
        private IExitableState _activeState;

        [Inject]
        public GameStateMachine(IStateFactory statesFactory)
        {
            _statesFactory = statesFactory;
        }

        public void InitializeStateMachine()
        {
            _states = new Dictionary<Type, IExitableState>()
            {
                [typeof(BootstrapState)] = _statesFactory.Create<BootstrapState>(),
                [typeof(LoadSceneState)] = _statesFactory.Create<LoadSceneState>(),
                [typeof(GameState)] = _statesFactory.Create<GameState>()
            };
        }

        public void Enter<TState>() where TState : class, IState
        {
            IState state = ChangeState<TState>();
            state.Enter();
        }

        public void Enter<TState, TPayload>(TPayload payload) where TState : class, IPayLoadedState<TPayload>
        {
            IPayLoadedState<TPayload> state = ChangeState<TState>();
            state.Enter(payload);
        }

        private TState ChangeState<TState>() where TState : class, IExitableState
        {
            _activeState?.Exit();

            TState state = GetState<TState>();
            _activeState = state;

            return state;
        }

        private TState GetState<TState>() where TState : class, IExitableState
        {
            return _states[typeof(TState)] as TState;
        }
    }
}