using _Project.Scripts.StateMachine;
using Zenject;

namespace _Project.Scripts.Factories
{
    public class StatesFactory : IStateFactory
    {
        private readonly IInstantiator _instantiator;
        
        [Inject]
        public StatesFactory(IInstantiator instantiator)
        {
            _instantiator = instantiator;
        }
        
        public T Create<T>() where T : IExitableState
        {
            return _instantiator.Instantiate<T>();
        }
    }
}