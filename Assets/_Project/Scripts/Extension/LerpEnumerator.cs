using System;
using System.Collections;
using UnityEngine;

namespace _Project.Scripts.Extension
{
    public static class LerpEnumerator
    {
        public static IEnumerator OnUpdate(float duration, Action<float> action, Action onFinished)
        {
            float startTime = Time.time;

            float t;
            while ((t = (Time.time - startTime) / duration) <= 1f)
            {
                action?.Invoke(t);
                yield return null;
            }

            action?.Invoke(1f);
            onFinished?.Invoke();
        }
    }
}
