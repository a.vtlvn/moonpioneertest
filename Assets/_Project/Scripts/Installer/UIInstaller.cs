using _Project.Scripts.UI;
using Zenject;

namespace _Project.Scripts.Installer
{
    public class UIInstaller : MonoInstaller<UIInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<InfoText>().FromComponentInHierarchy().AsSingle();
        }
    }
}
