using _Project.Scripts.Factories;
using _Project.Scripts.Services.Input;
using _Project.Scripts.StateMachine;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.Installer
{
    public class GlobalInstaller : MonoInstaller, IInitializable
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<GameStateMachine>().AsSingle();
            Container.BindInterfacesAndSelfTo<StatesFactory>().AsSingle();
            RegisterInputService();
            BindThis();
        }

        public void Initialize()
        {
            Container.Resolve<IGameStateMachine>().InitializeStateMachine();
            Container.Resolve<IGameStateMachine>().Enter<BootstrapState>();
        }

        private void BindThis()
        {
            Container.BindInterfacesAndSelfTo<GlobalInstaller>().FromInstance(this).AsSingle();
        }
        
        private void RegisterInputService()
        {
            if (Application.isEditor)
                Container.BindInterfacesAndSelfTo<StandaloneInputService>().AsSingle();
            else
                Container.BindInterfacesAndSelfTo<MobileInputService>().AsSingle();
        }
    }
}